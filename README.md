# teste-frontend

Olá Candidado!

A ideia desse testes é se aproximar a praticas no seu dia-a-dia aqui na mob. Reproduzindo um login simplificado e um crud com listagem.

## Histórias

* Como usuário preciso fazer o login para poder usar de maneira segura o sistema.
* Como usuário preciso gerenciar os visitantes em minha rede, para que possa controlar seus acessos em minhas lojas.

## Requisitos

### Login

* Logar se o login e senha forem válidos

### Busca de Visitante

* Só pode ser acessada depois de logar
* Permitir busca com nome do visitante.

### Visitante

* Só pode ser acessada depois de logar
* O formulário precisa ter validação de validade de cpf
* Os formulários tem que controle de obrigatóriedade para campos fazios.
* Preencher automaticamente o endereço com o preenchimento do CEP (pode ser usando o serviço https://viacep.com.br/ws/13289188/json/)

## Premissas 

* Utilizar React

## Extras

* Typescript
* Sass

## Protótipo

https://xd.adobe.com/view/fa847a2c-bac2-4ca5-6182-fd4089679b78-8cb5/